#include <stdlib.h>
#include "ArrayList.h"
#include <limits.h>
#include <stdio.h>
#include <string.h>

struct list* initialize(int initialSize) {
  int i = 0;
  struct list* newList = (struct list*) malloc (sizeof(struct list));
  newList->length = initialSize;
  
  newList->address = (int*) malloc(initialSize * sizeof(int));
  
  for (i = 0; i < initialSize; i++) {
	*(newList->address + i) = INT_MIN;
  }
  return newList;
}

void set(struct list* lst, int index, int value) {
  if (index < lst->length) {
	*(lst->address + index) = value;
  }
  else {
	int* oldAddress = lst->address;
	lst->address = (int*) malloc((index+1) * sizeof(int));
	memcpy(lst->address, oldAddress, lst->length * sizeof(int));
	*(lst->address + index) = value;
	free(lst->address);
	lst->length = index+1;
  }
}

int get(struct list* lst, int index) {
	if (index >= lst->length) {
		fprintf (stderr, "Error: attempt to access index %d which is outside current valid range of [0 - %d]\n", index, lst->length-1);
		return INT_MIN;
	}
	
	return *(lst->address + index);
}

/*
  Reduces the size of the list pointed to by lst 
  to the given size; any data that exists with index -size- or 
  greater are lost
*/
void truncate(struct list* lst, int size) {
	int* newSpace = (int*) malloc (size * sizeof(int));
	int i = 0;
	int j = 0;
	for (i = 0; i < lst->length; i++) {
		*(newSpace+j) = *(lst->address+i);
		j++;
	}
	lst->address = newSpace;
	lst->length = size;

	free(lst->address);
}

/*
  Reduces the amount of memory being used by the list to match its size,
  by creating a new block of memory to hold exactly the right number of 
  elements, copying the elements of the existing list that have been set,
  and then freeing up the memory previously allocated to the list
*/
void compact (struct list* lst) {
	int numElements = 0;
	int i = 0;
	int j = 0;
	for (i = 0; i < lst->length; i++) {
		if (*(lst->address+i) != INT_MIN) {
			numElements++;
		}
	}

	int* newSpace = (int*) malloc(numElements * sizeof(int));
	for (i = 0; i < lst->length; i++) {
		if (*(lst->address+i) != INT_MIN) {
			*(newSpace+j) = *(lst->address+i);
			j++;
		}

	}
	
	free(lst->address);
	lst->length = numElements;
	lst->address = newSpace;
}
