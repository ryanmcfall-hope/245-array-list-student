struct list {
  int* address;
  int length;
 };

void set(struct list* lst, int index, int value);

/*
  Retrieves the string stored in the list at position index
  Returns NULL if the index is 
*/
int get(struct list* lst, int index);

/*
  Creates a new list capable of holding the number of entries
  specified in the parameter initialSize
*/
struct list* initialize(int initialSize);

/*
  Reduces the size of the list pointed to by lst 
  to the given size; any data that exists with index -size- or 
  greater are lost
*/
void truncate(struct list* lst, int size);

/*
  Reduces the amount of memory being used by the list to match its size,
  by creating a new block of memory to hold exactly the right number of 
  elements, copying the elements of the existing list that have been set,
  and then freeing up the memory previously allocated to the list
*/
void compact (struct list* lst);
