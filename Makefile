main: main.o ArrayList.o
	gcc -o main main.o ArrayList.o

ArrayList.o: ArrayList.c
	gcc -c -g ArrayList.c

main.o: main.c
	gcc -c -g main.c

clean:
	rm main ArrayList.o main.o
